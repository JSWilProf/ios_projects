//
//  TestaAlbuns.swift  
//  AlbunsTests
//
//  Created by Professor.
//  Copyright © 2021 Senai. All rights reserved.
//  

import XCTest
@testable import Albuns

class TestaAlbuns: XCTestCase {
    let dataBase = DataBaseController.sharedInstance
   
    private func criaAlbum(_ nome: String,_ banda: String, _ ano: String, _ estilo: String, _ capa: UIImageView?, _ url: String) throws -> Album? {
        let album = dataBase.novoAlbum()
        album.nome = nome
        album.banda = banda
        album.ano = ano.numberValue
        album.estilo = estilo
        album.capa = capa?.image?.pngData()
        album.url = url
        try dataBase.salvar()
        
        return album
    }
    
    override func setUpWithError() throws {
        try dataBase.albuns()?.forEach { album in
            try dataBase.delete(album)
        }
        
        let capa = UIImageView(image: UIImage(systemName: "disco"))
        
        _ = try criaAlbum("Empty Words", "Alix Perez", "2014", "Jungle", capa, "http://www.google.com.br")
        _ = try criaAlbum("Stay Like This", "Maduk", "2021", "Drum’n’bass", capa, "http://www.google.com.br")
    }
    
    override func tearDownWithError() throws {
        func removeAlbum(_ nome: String) throws {
            if let album = try dataBase.localizar(nome: nome) {
                try dataBase.delete(album)
            }
        }
        
        do { try removeAlbum("Rise from the Ashe") } catch {}
        do { try removeAlbum("Empty Words") } catch {}
        do { try removeAlbum("Stay Like This") } catch {}
    }

    func testCriaAlbum() throws {
        let capa = UIImageView(image: UIImage(systemName: "disco_teste"))
        
        let album = try criaAlbum("Rise from the Ashe", "The Ghost Inside", "2016", "Rock", capa, "http://oi.com.br")
        
        let oAlbum = try dataBase.localizar(nome: "Rise from the Ashe")
        
        XCTAssertTrue(oAlbum != nil, "Álbum não encontrado")
        XCTAssertEqual(album?.nome, oAlbum!.nome, "Nomes diferentes")
        XCTAssertEqual(album?.capa, oAlbum!.capa, "Capas dos objetos diferentes")
        XCTAssertEqual(capa.image?.pngData(), oAlbum!.capa, "Capas UIImageView diferentes")
    }
    
    func testLocalizaAlbum() throws {
        let album = try dataBase.localizar(nome: "Stay Like This")
        XCTAssertTrue(album != nil, "O Álbum não foi encontrado")
    }
    
    func testRemoveAlbum() throws {
        var album = try dataBase.localizar(nome: "Empty Words")
        XCTAssertTrue(album != nil, "O Álbum não foi encontrado")
        
        try dataBase.delete(album!)
        album = try dataBase.localizar(nome: "Empty Words")
        XCTAssertTrue(album == nil, "O Álbum não foi removido")
    }
    
    func testListarAlbuns() throws {
        let lista = try dataBase.albuns()
        XCTAssertTrue(lista?.count == 2, "Número inválido de álbuns cadastrados")
    }
}
