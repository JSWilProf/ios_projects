//
//  AlbunsUITests.swift  
//  AlbunsUITests
//
//  Created by Professor.
//  Copyright © 2021 Senai. All rights reserved.
//  

import XCTest
//@testable import Albuns

class AlbunsUITests: XCTestCase {
//    let dataBase = DataBaseController.sharedInstance
//    let store = PListController.sharedInstance
    
    override func setUpWithError() throws {
        continueAfterFailure = false
        
//        try store.removeLogin()
//        try dataBase.albuns()?.forEach { album in
//            try dataBase.delete(album)
//        }
    }

    override func tearDownWithError() throws {
    }

    func testExample() throws {
        /// Inicia a aplicação
        let app = XCUIApplication()
        app.launch()
        
        /// Aguarda a aplicação iniciar
        let botaoCadastrar = app.staticTexts["Cadastrar"]
        let botaoEntrar = app.staticTexts["Entrar"]
        _ = botaoEntrar.waitForExistence(timeout: 3)
        
        var loginNovo = false
        
        /// Tenta cadastrar nova conta de acesso
        if botaoCadastrar.exists {
            botaoCadastrar.tap()
            let tablesQuery = app.tables
            let cadLoginTextField = tablesQuery.textFields["cad_login"]
            cadLoginTextField.tap()
            cadLoginTextField.typeText("teste1")
            
            let cadSenhaTextField = tablesQuery.secureTextFields["cad_senha"]
            cadSenhaTextField.tap()
            cadSenhaTextField.typeText("teste1")
            cadLoginTextField.tap()
            tablesQuery.buttons["Salvar"].tap()
            
            loginNovo = true
        } else {
            let switchFaceId = app.switches["faceId"]
            if switchFaceId.exists {
                switchFaceId.swipeLeft()
            }
        }
        
        /// Efetua o login
        let loginTextField = app.textFields["login"]
        loginTextField.tap()
        if loginNovo {
            loginTextField.typeText("teste1")
        }
        let senhaTextField = app.secureTextFields["senha"]
        senhaTextField.tap()
        senhaTextField.typeText("teste1")
        loginTextField.tap()
        app.staticTexts["Entrar"].tap()
        
        let novo = app.navigationBars["Álbuns"].buttons["Novo"]
        _ = novo.waitForExistence(timeout: 1)
        
        /// Verifica se não existem albuns cadastrados
        let alerta = app.alerts["Sem Álbuns cadastrados"]
        if alerta.exists {
            alerta.scrollViews.otherElements.buttons["Fechar"].tap()
            novo.tap()
            
            /// Cadastra um album
            let tablesQuery = app.tables
            let nome = tablesQuery.textFields["album_nome"]
            nome.tap()
            nome.typeText("Elysium")
            let banda = tablesQuery.textFields["album_banda"]
            banda.tap()
            banda.typeText("Jo Blankenburg")
            let estilo = tablesQuery.textFields["album_estilo"]
            estilo.tap()
            estilo.typeText("Clássico")
            let ano = tablesQuery.textFields["album_ano"]
            ano.tap()
            ano.typeText("2014")
            let url = tablesQuery.textFields["album_url"]
            url.tap()
            url.typeText("https://youtu.be/BPfzWZsszcw")
            app.navigationBars["Álbum"].buttons["Salvar"].tap()
        }
        
        /// Apaga o album
        let albunsNavigationBar = app.navigationBars["Álbuns"]
        albunsNavigationBar.buttons["Editar"].tap()
        let tablesQuery = app.tables
        
        tablesQuery.buttons["Delete Elysium, Jo Blankenburg"].tap()
        tablesQuery.buttons["Delete"].tap()
        app.sheets["Atenção"].scrollViews.otherElements.buttons["Confirma"].tap()
        albunsNavigationBar.buttons["Concluir"].tap()
    }

    func textLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
