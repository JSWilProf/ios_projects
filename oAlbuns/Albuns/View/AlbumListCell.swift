//
//  AlbumListCell.swift  
//  Albuns
//
//  Created by Professor.
//  Copyright © 2021 Senai. All rights reserved.
//  

import Foundation
import UIKit

class AlbumListCell: UITableViewCell {
    @IBOutlet var albumLabel: UILabel!
    @IBOutlet var bandaLabel: UILabel!
    @IBOutlet weak var capaImg: UIImageView!
    
    func configure(album: String, banda: String, capa: Data?) {
        albumLabel?.text = album
        bandaLabel?.text = banda
        if let aCapa = capa {
            capaImg.image = UIImage(data: aCapa)
        }
    }
}
