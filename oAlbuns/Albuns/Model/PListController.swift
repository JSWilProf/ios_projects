//
//  PListController.swift  
//  Albuns
//
//  Created by Professor.
//  Copyright © 2021 Senai. All rights reserved.
//  

import Foundation

struct Login: Codable {
    var login: String
    var senha: String
    var faceId: Bool
}

class PListController: NSObject {
    static let sharedInstance = PListController()
    
    fileprivate override init() {
        super.init()
    }
    
    private lazy var applicationDocumentsDirectory: URL = {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("login.plist")
    }()
    
    func getLogin() -> Login? {
        if let file = FileManager.default.contents(atPath: self.applicationDocumentsDirectory.path),
           let login = try? PropertyListDecoder().decode(Login.self, from: file) {
            return login
        } else {
            return nil
        }
    }
    
    func setLogin(_ login: Login) throws {
        let encoder = PropertyListEncoder()
        encoder.outputFormat = .binary
        
        do {
            let data = try encoder.encode(login)
            try data.write(to: self.applicationDocumentsDirectory)
        } catch {
            print(error)
            throw DBError.falhaAoSalvarDados
        }
    }
    
    func removeLogin() throws {
        do {
            try FileManager.default.removeItem(atPath: self.applicationDocumentsDirectory.path)
        } catch {
            print(error)
            throw DBError.falhaEmApagarDados
        }
    }
}
