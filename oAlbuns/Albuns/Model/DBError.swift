//
//  DBError.swift  
//  Albuns
//
//  Created by Professor.
//  Copyright © 2021 Senai. All rights reserved.
//  

import Foundation
enum DBError: Error {
    case falhaEmLerAlbum
    case falhaEmApagarAlbum
    case falhaAoSalvarDados
    case falhaEmApagarDados
}
