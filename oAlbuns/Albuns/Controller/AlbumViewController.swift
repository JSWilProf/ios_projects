//
//  AlbumViewController.swift  
//  Albuns
//
//  Created by Professor.
//  Copyright © 2021 Senai. All rights reserved.
//  

import Foundation
import UIKit

protocol AlbumModelChangedDelegate {
    func albumModelChanged()
}

class AlbumViewController: UITableViewController, AlbumModelChangedDelegate {
    static let detalheIdentifier = "ShowAlbumDetalheSegue"
    let databaseController = DataBaseController.sharedInstance
    var albuns: [Album]!
    enum Estado {
        case normal
        case vazio
        case erro
    }
    var estado = Estado.vazio
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        do {
            albuns = try databaseController.albuns()
            
            if albuns != nil {
                estado = .normal
                self.navigationItem.rightBarButtonItem = self.editButtonItem
                editButtonItem.title = "Editar"
            }
        } catch {
            estado = .erro
        }
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.editButtonItem.title = editing ? "Concluir" : "Editar"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        switch estado {
            case .vazio:
                alert("Sem Álbuns cadastrados", message: "Não existem Álbuns cadastrados até o momento")
            case .erro:
                alert("Falha ao acessar os dados", message: "Os dados dos Álbuns não puderam ser recuperados")
            default:
                break
        }
    }

    func albumModelChanged() {
        do {
            albuns = try databaseController.albuns()
            tableView.reloadData()
        } catch {
            alert("Falha ao acesso aos dados", message: "Os dados dos Álbuns não puderam ser recuperados")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Voltar"
        navigationItem.backBarButtonItem = backItem
        
        if segue.identifier == Self.detalheIdentifier,
           let destination = segue.destination as? AlbumDetalheViewController {
            if let cell = sender as? UITableViewCell,
               let indexPath = tableView.indexPath(for: cell) {
                let album = albuns[indexPath.row]
                destination.configure(with: album, delegate: self)
            } else {
                destination.configure(with: nil, delegate: self)
            }
        }
    }

    @IBAction func incluir(_ sender: Any) {
        performSegue(withIdentifier: Self.detalheIdentifier, sender: self)
    }
}

extension AlbumViewController {
    static let identifier = "AlbumListCell"
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albuns != nil ? albuns.count : 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell( withIdentifier: Self.identifier, for: indexPath) as? AlbumListCell else {
            fatalError("Erro ao localizar o \(Self.identifier)")
        }
        
        let album = albuns[indexPath.row]
        cell.configure(album: album.nome!, banda: album.banda!, capa: album.capa)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // cria um alerta
            let alert = UIAlertController(title: "Atenção", message: "Confirma a exclusão do Álbum?", preferredStyle: .actionSheet)
            // adiciona os botões das ações
            alert.addAction(UIAlertAction(title: "Confirma", style: .destructive, handler: { [self] (action: UIAlertAction) -> Void in
                do {
                    try databaseController.delete(albuns[indexPath.row])
                    albuns = try databaseController.albuns()
                    tableView.beginUpdates()
                    tableView.deleteRows(at: [indexPath], with: .left)
                    tableView.endUpdates()
                } catch {
                    self.alert("Fala ao acesso aos dados", message: "A ação não pode ser executada por problemas no armazenamento dos dados")
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancela", style: .cancel, handler:  nil))
            
            // apresenta o alert
            self.present(alert, animated: true, completion: nil)
        }
    }
}
