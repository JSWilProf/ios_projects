//
//  FilmeListCell.swift
//  Albuns
//
//  Created by Mauricio Cantuaria.
//  Copyright © 2022 Senai. All rights reserved.
//

import Foundation
import UIKit

/// Classe que representa um Álbum a ser apresentado na lista da tela principal
class FilmeListCell: UITableViewCell {
    /// Declaração das referências aos componentes da tela
    @IBOutlet var tituloLabel: UILabel!
    @IBOutlet var anoLabel: UILabel!
    @IBOutlet weak var capaImg: UIImageView!
    
    /// Método utilizado para atribuir os valores aos campos do item da lista
    func configure(titulo: String, ano: Int64, capa: Data?) {
        tituloLabel?.text = titulo
        anoLabel?.text = String(ano)
        if let aCapa = capa {
            capaImg.image = UIImage(data: aCapa)
        }
    }
}
