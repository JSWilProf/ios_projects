//
//  FilmeViewController.swift
//  Albuns
//
//  Created by Mauricio Cantuaria.
//  Copyright © 2022 Senai. All rights reserved.
//

import Foundation
import UIKit

/// Protocolo que define o padrão de comunicação entre as
/// Classes que controlam o Detalhe de Álbum e Lista de Álbuns
protocol FilmeModelChangedDelegate {
    /// Definição de método com o intúito de permitir informar
    /// o evento de alteração de dados de um determinado
    /// Álbum que foi editado na Tela de Detalhe
    func filmeModelChanged()
}

class FilmeViewController: UITableViewController, FilmeModelChangedDelegate {
    /// Identificação da ligação para Tela de Detalhe utilizada em sua navegação
    static let detalheIdentifier = "ShowFilmeDetalheSegue"
    /// Referência ao Gerenciador de dados
    let databaseController = DataBaseController.sharedInstance
    /// Lista de álbuns a ser apresentada na tela
    var filmes: [Filme]!
    /// Definição possíveis estados para controle da apresentação da lista de álbuns
    enum Estado {
        case normal
        case vazio
        case erro
    }
    var estado = Estado.vazio
    
    /// Método que recebe o evento de inicialização da Tela principal
    /// e consulta a lista de álbuns para apresentar na lista da tela
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        do {
            filmes = try databaseController.filmes()
            
            if filmes != nil {
                estado = .normal
                self.navigationItem.rightBarButtonItem = self.editButtonItem
                editButtonItem.title = "Editar"
            }
        } catch {
            estado = .erro
        }
    }
    
    /// Método que apresenta o texto do botão no tompo superior direito da tela
    /// do aplicativo
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.editButtonItem.title = editing ? "Concluir" : "Editar"
    }
    
    /// Método que revebe o evento de finalização da apresentação da tela principal
    /// e dependendo do estado da carga da lista de álbuns, apresenta mensagem
    /// correspondente
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        switch estado {
            case .vazio:
                alert("Sem Filmes cadastrados", message: "Não existem Filmes cadastrados até o momento")
            case .erro:
                alert("Falha ao acessar os dados", message: "Os dados dos Filmes não puderam ser recuperados")
            default:
                break
        }
    }

    /// Método correspondente ao protocolo que recebe o evento de
    /// alterações em dados de Álbum editado na tela de Detalhe
    /// e na ocorrência deste evento recarrega a lista dos álbuns
    /// na tela principal
    func filmeModelChanged() {
        do {
            filmes = try databaseController.filmes()
            tableView.reloadData()
        } catch {
            alert("Falha ao acesso aos dados", message: "Os dados dos Filmes não puderam ser recuperados")
        }
    }
    
    /// Método que trata a navegação para a tela de Cadastro para um novo Álbum e
    /// para alterar dados de um Álbum existente.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Voltar"
        navigationItem.backBarButtonItem = backItem
        
        if segue.identifier == Self.detalheIdentifier,
           let destination = segue.destination as? FilmeDetalheViewController {
            if let cell = sender as? UITableViewCell,
               let indexPath = tableView.indexPath(for: cell) {
                let filme = filmes[indexPath.row]
                destination.configure(with: filme, delegate: self)
            } else {
                destination.configure(with: nil, delegate: self)
            }
        }
    }

    /// Método vinculado ao botão para incluir um  novo Álbum
    @IBAction func incluir(_ sender: Any) {
        performSegue(withIdentifier: Self.detalheIdentifier, sender: self)
    }
}

/// Extensão que implementa as funcionalidades para controlar os
/// TableView onde são apresentadas a lista dos álbuns.
extension FilmeViewController {
    /// Identificação da Célula que apresenta dados dos itens na lista principal
    static let identifier = "FilmeListCell"
    
    /// Método que informa a quantidade de elementos na lista de álbuns
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filmes != nil ? filmes.count : 0
    }
    
    /// Método que constroi e carrega cada célula apresentada na lista de álbuns
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell( withIdentifier: Self.identifier, for: indexPath) as? FilmeListCell else {
            fatalError("Erro ao localizar o \(Self.identifier)")
        }
        
        let filme = filmes[indexPath.row]
        cell.configure(titulo: filme.titulo!, ano: filme.ano, capa:filme.capa)
        return cell
    }
    
    /// Método que trata da exclusão de um item na lista de álbuns
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // cria um alerta
            let alert = UIAlertController(title: "Atenção", message: "Confirma a exclusão do Filme?", preferredStyle: .actionSheet)
            // adiciona os botões das ações
            alert.addAction(UIAlertAction(title: "Confirma", style: .destructive, handler: { [self] (action: UIAlertAction) -> Void in
                do {
                    try databaseController.delete(filmes[indexPath.row])
                    filmes = try databaseController.filmes()
                    tableView.beginUpdates()
                    tableView.deleteRows(at: [indexPath], with: .left)
                    tableView.endUpdates()
                } catch {
                    self.alert("Falha ao acesso aos dados", message: "A ação não pode ser executada por problemas no armazenamento dos dados")
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancela", style: .cancel, handler:  nil))
            
            // apresenta o alert
            self.present(alert, animated: true, completion: nil)
        }
    }
}
