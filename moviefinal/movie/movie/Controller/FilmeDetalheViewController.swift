//
//  FilmeDetalheViewController.swift
//  Albuns
//
//  Created by Mauricio Cantuaria.
//  Copyright © 2022 Senai. All rights reserved.
//  

import Foundation
import UIKit

class FilmeDetalheViewController: UITableViewController {
    /// Referência utilizada para edição de um Filme
    private var filme: Filme?
    /// Referência para o acesso ao Objeto que receberá o evento de edição do álbum
    private var modelDelegate: FilmeModelChangedDelegate?
    /// Referência ao controlador do componente para a obtenção da Foto
    private var imagePicker = UIImagePickerController()
    /// Declaração das variáveis utilziadas globalmente nesta classe
    private var atualizaFoto = false
    
    /// Declaração das referências aos componentes da tela
    @IBOutlet weak var aCapa: UIImageView!
    @IBOutlet weak var oTitulo: UITextField!
    @IBOutlet weak var oDiretor: UITextField!
    @IBOutlet weak var oPais: UITextField!
    @IBOutlet weak var oAno: UITextField!
    @IBOutlet weak var oTrailler: UITextField!
    @IBOutlet weak var abrirVideo: UIButton!
 
    /// Inicializa o View Controller
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registraKeyboard()
        configuraTouch()
        confiuraCampos()
    }
    
    /// Método que recebe o evento de inicialização da Tela de Cadastro
    /// verifica se é para editar um álbum existente ou um novo álbum
    override func viewWillAppear(_ animated: Bool) {
        if let filme = filme {
            if let cadaData = filme.capa, !atualizaFoto {
                aCapa.image = UIImage(data: cadaData)
            }
            oTitulo.text = filme.titulo
            oDiretor.text = filme.diretor
            oPais.text = filme.pais
            oAno.text = filme.ano.description
            if let url = filme.trailler {
                oTrailler.text = url
                abrirVideo.isEnabled = url.count > 0
            }
        }
    }

    /// Método utilizado para a configuração desta classe
    func configure(with filme: Filme?, delegate: FilmeModelChangedDelegate) {
        self.filme = filme
        self.modelDelegate = delegate
    }

    /// Método que abre o navegador para a URL contida do atributo video
    @IBAction func abrirVideo(_ sender: Any) {
        if let oEndereco = URL(string: oTrailler.text!) {
            UIApplication.shared.open(oEndereco)
        }
    }
    
    /// Método utilizado para salvar os dados do Filme editado no banco de dados
    /// interno do dispositivo móvel
    @IBAction func salvar(_ sender: Any) {
        do {
            let databaseController = DataBaseController.sharedInstance
            if let velhoFilme = filme {
                velhoFilme.titulo = oTitulo.text!
                velhoFilme.diretor = oDiretor.text!
                velhoFilme.ano = oAno.text!.numberValue
                velhoFilme.pais = oPais.text!
                velhoFilme.capa = aCapa.image?.pngData()
                velhoFilme.trailler = oTrailler.text!
            } else {
                let novoFilme = databaseController.novoFilme()
                novoFilme.titulo = oTitulo.text!
                novoFilme.diretor = oDiretor.text!
                novoFilme.ano = oAno.text!.numberValue
                novoFilme.pais = oPais.text!
                novoFilme.capa = aCapa.image?.pngData()
                novoFilme.trailler = oTrailler.text!
            }
            try databaseController.salvar()
            modelDelegate?.filmeModelChanged()
            _ = navigationController?.popToRootViewController(animated: true)
        } catch {
            alert("Falha ao gravar", message: "O Filme não pode ser gravado")
        }
    }
}

/// Extensão utilizada para controlar os TextFields (Campos de edição) e sua navegação
extension FilmeDetalheViewController: UITextFieldDelegate {
    /// Ativa o gerenciamento de todos os campos da tela
    func confiuraCampos() {
        oTitulo.delegate = self
        oDiretor.delegate = self
        oPais.delegate = self
        oAno.delegate = self
        oTrailler.delegate = self
    }
    
    /// Método que determina quando o botão abrirVideo ficará habilitado
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == oTrailler {
            abrirVideo.isEnabled = oTrailler.text!.count > 0
        }
    }
    
    /// Método que controla a sequencia de navegação dos campos na tela
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
            case oTitulo:
                oDiretor.becomeFirstResponder()
            case oDiretor:
                oPais.becomeFirstResponder()
            case oPais:
                oAno.becomeFirstResponder()
            case oAno:
                oTrailler.becomeFirstResponder()
            default:
                textField.resignFirstResponder()
        }
        return true
    }
}

/// Extensão utilizada para configurar o TableViewController
extension FilmeDetalheViewController {
    /// Configura o cabeçalho do Table View removendo o espaço acima dos itens da lista
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    /// Configura o rodapé do Table View removendo o espaço abaixo dos itens da lista
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}

/// Extensão que habilita o reconhecimento de gestos na tela do dispositivo móvel e
/// a obtenção da imagem da galeria de fotos.
extension FilmeDetalheViewController: UIImagePickerControllerDelegate,
                                      UINavigationControllerDelegate {
    /// Método para habilitar o reconhecimento de gestos sobre a imagem na tela
    func configuraTouch() {
        let singleTap = UITapGestureRecognizer(
            target: self,
            action: #selector(fromGallery))
        singleTap.numberOfTapsRequired = 1
        singleTap.numberOfTouchesRequired = 1
        aCapa.addGestureRecognizer(singleTap)
        aCapa.isUserInteractionEnabled = true
    }
    
    /// Método que abre o seletor de imagens da galeria de fotos
    @objc func fromGallery() {
        if UIImagePickerController
            .isSourceTypeAvailable(.photoLibrary) {
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    /// Método que captura a imagem selecionada e fecha o seletor de imagens
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info:
                               [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.editedImage] as? UIImage {
            atualizaFoto = true
            self.aCapa.image = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
