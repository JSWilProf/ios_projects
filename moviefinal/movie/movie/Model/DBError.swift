//
//  DBError.swift
//  Albuns
//
//  Created by Mauricio Cantuaria.
//  Copyright © 2022 Senai. All rights reserved.
//

import Foundation

/// Enumeração que descreve os possíveis erros
/// que possam ocorrer na aplicação
enum DBError: Error {
    case falhaEmLerFilme
    case falhaEmApagarFilme
    case falhaAoSalvarDados
}
